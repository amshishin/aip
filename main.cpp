#include <iostream>
using namespace  std;


template<typename T>
class Matrix
{
    int rows;
    int cols;
    T* matrix;
public:
    Matrix(int rows_)
    {
        rows = rows_;
        cols = 1;
        matrix = new T [rows];
    }

    Matrix(int rows_, int cols_)
    {
        rows = rows_;
        cols = cols_;
        matrix = new T [rows*cols];
    }

    Matrix(const initializer_list<T> &lst)
    {
        rows = 1;
        cols = lst.size();
        matrix = new T [rows*cols];
        for (int i = 0; i < rows*cols; i++)
        {
            matrix[i] = *(lst.begin() + i);
        }
    }

    Matrix(const initializer_list<initializer_list<T>> &lst)
    {
        rows = lst.size();
        cols = lst.begin()->size();
        matrix = new T [rows*cols];
        for (int i = 0; i < rows; i++)
        {
            for (int j=0; j < cols; j++)
            {
                matrix[i * cols + j] = *((lst.begin() + i)->begin() + j);
            }
        }
    }

    Matrix(const Matrix<T> & other)
    {
        this->rows = other.rows;
        this->cols = other.cols;
        this->matrix = new T [this->rows * this->cols];
        for (int i = 0; i < this->rows * this->cols; i++)
        {
            this->matrix[i] = other.matrix[i];
        }
    }

    Matrix(Matrix<T> && other)
    {
        this->rows = other.rows;
        this->cols = other.cols;
        this->matrix = new T [this->rows* this->cols];
        for (int i = 0; i < this->rows * this->cols; i++)
        {
            this->matrix[i] = other.matrix[i];
        }
        other.rows = 0;
        other.cols = 0;
    }

    void Output()
    {
        int k = 0;
        for (int i = 0; i < this->rows; i++)
        {
            for (int j = 0; j < this->cols; j++)
            {
                cout << this->matrix[k] << "\t";
                k++;
            }
            cout << endl;
        }
        cout << endl;
    }

    T& operator () (const int & r, const int & c)
    {
        return matrix[r * this->cols + c];
    }

    Matrix<T> operator + (const Matrix<T> & other)
    {
        if (this->cols != other.cols or this->rows != other.rows)
        {
            cerr << "the size of the matrices are different" << endl;
            throw -1;
        }
        Matrix<T> res(this->rows, this->cols);
        for (int i = 0; i < this->rows * this->cols; i++)
        {
            res.matrix[i] = this->matrix[i] + other.matrix[i];
        }
        return res;
    }

    Matrix<T> operator - (const Matrix<T> & other)
    {
        if (this->cols != other.cols or this->rows != other.rows)
        {
            cerr << "the size of the matrices are different" << endl;
            throw -1;
        }
        Matrix<T> res(this->rows, this->cols);
        for (int i = 0; i < this->rows * this->cols; i++)
        {
            res.matrix[i] = this->matrix[i] - other.matrix[i];
        }
        return res;
    }

    Matrix<T> operator * (const T & num)
    {
        Matrix<T> res(this->rows, this->cols);
        for (int i = 0; i < this->rows * this->cols; i++)
        {
            res.matrix[i] = this->matrix[i] * num;
        }
        return res;
    }

    Matrix<T> operator * (const Matrix<T> & other)
    {
        if (this->cols != other.rows)
        {
            cerr << "matrix sizes are not allowed for multiplication" << endl;
            throw -1;
        }
        int k = 0;
        Matrix<T> res(this->rows, other.cols);
        for (int i = 0; i < this->rows; i++)
        {
            for (int j = 0; j < other.cols; j++)
            {
                T sum = 0;
                for (int s = 0; s < this->cols; s++)
                {
                    sum += this->matrix[i * this->cols + s] * other.matrix[s * other.cols + j];
                }
                res.matrix[k] = sum;
                k++;
            }
        }
        return res;
    }

    void operator += (const Matrix<T> & other)
    {
        if (this->cols != other.cols or this->rows != other.rows)
        {
            cerr << "the size of the matrices are different" << endl;
            throw -1;
        }
        for (int i = 0; i < this->rows * this->cols; i++)
        {
            this->matrix[i] += other.matrix[i];
        }
    }

    void operator -= (const Matrix<T> & other)
    {
        if (this->cols != other.cols or this->rows != other.rows)
        {
            cerr << "the size of the matrices are different" << endl;
            throw -1;
        }
        for (int i = 0; i < this->rows * this->cols; i++)
        {
            this->matrix[i] = this->matrix[i] - other.matrix[i];
        }
    }

    void operator *= (const T & num)
    {
        for (int i = 0; i < this->rows * this->cols; i++)
        {
            this->matrix[i] *= num;
        }
    }

    ~Matrix()
    {
        delete [] matrix;
    }
};


int main()
{
    //задаём матрицы
    Matrix<int> m1 = {1,2,3,4,5,6};
    m1.Output();
    Matrix<int> m2 = {{1,2,3,4,5,6}};
    m2.Output();
    Matrix<int> m3 = {{1,2,3},{4,5,6}};
    m3.Output();
    Matrix<int> m4 = {{1,2},{3,4},{5,6}};
    m4.Output();
    Matrix<int> m5 = {{1,2},{3,4}};
    m5.Output();

    //конструкторы копирования и перемещения
    Matrix<int> copy(m1);
    Matrix<int> mov(std::move(m1));
    copy.Output();
    mov.Output();
    m1.Output(); // выводится пустая строка

    //умножение матриц
    Matrix<int> mult = m3 * m4;
    mult.Output();

    //сложение матриц
    Matrix<int> sum = copy + m2;
    sum.Output();

    //вычитание матриц
    Matrix<int> dif = copy * 5 - m2;
    dif.Output();

    //умножение на число
    Matrix<int> res = copy * 3;
    res.Output();

    //обращение к элементу матрицы
    cout << mult(1,1) << endl<< endl;
    mult(1,1) = 87;
    cout << mult(1,1) << endl<< endl;

    // операторы
    mult += m5;
    mult.Output();
    mult-= m5;
    mult.Output();
    mult*= -1;
    mult.Output();
}
