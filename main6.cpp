#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <cmath>

using namespace std;

template <typename TKey, typename TValue>

class HashTable
{

private:
    int capacity = 73;
    int size = 0;
    vector<list<pair<TKey, TValue>>> table;

public:

    HashTable ()
    {
        table = vector<list<pair<TKey, TValue>>>(100);
    }


    void Insert(const TKey& key, const TValue & value)
    {
        size_t pos = HashFunc(key);
        pair <TKey, TValue> pair (key, value);
        table[pos].push_back(pair);
        size++;
    }

    const TValue Search(const TKey & key)
    {
        size_t pos = HashFunc(key);
        for (auto elem: table[pos])
        {
            if (elem.first == key)
            {
                return elem.second;
            }
        }
    }

    void OutPut ()
    {
        cout << "Table now:" << endl << endl;
        for (auto list : table)
        {
            for (auto elem : list)
            {
                cout << elem.first << ' ' << elem.second << endl;
            }
        }
        cout << endl;
    }

    void Delete(const TKey & key)
    {
        size_t pos = HashFunc(key);
        int i = 0;
        auto it = table[pos].begin();
        for (const auto& elem: table[pos])
        {
            if (elem.first == key)
            {
                cout << "Deletion: " << endl << "key: " << key << endl << endl;
                advance(it, i);
                table[pos].erase(it);
                break;
            }
            i++;
        }
        size--;
    }

    const TValue operator[](const TKey& key)
    {
        size_t pos = HashFunc(key);
        for (const auto& elem: table[pos])
        {
            if (elem.first == key)
            {
                return elem.second;
            }
        }
    }

    size_t HashFunc (const string & key)
    {
        unsigned int p = 71;
        int M = capacity;

        unsigned long hash = 0;

        for (int i = 0; i < key.size(); i++)
        {
            hash += key[i]*(unsigned long)pow(p,i) % M;
        }
        hash = hash % M;
        return static_cast <size_t> (hash); // (size_t)hash - cast hash to size_t
    }

    ~HashTable()
    {
        table.clear();
    }

};

int main ()
{
    HashTable <string, string> myTable;
    myTable.Insert("Ivanov", "Ivan");
    myTable.Insert("Petrov", "Petr");
    myTable.OutPut();
    myTable.Insert("Vladimirov", "Vladimir");
    myTable.Insert("Sergeev", "Sergey");
    myTable.OutPut();
    cout << "Value by key: " << myTable["Ivanov"] << endl << endl;
    cout << "Searching:" << endl << "key: Petrov ---> value: " << myTable.Search("Petrov") << endl << endl;
    myTable.Delete("Vladimirov");
    myTable.OutPut();

}