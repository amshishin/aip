#include <iostream>
#include <algorithm>

using namespace std;

template<typename T, int max = 10>
        class Set {
private:
    int size;
    T* arr;
public:
    Set()
    {
        arr = new T[max];
    }

    Set(int _size, T* _arr)
    {
        arr = new T[_size];
        arr = _arr;
        size = _size;
        sort(arr, arr + size);
        T* result = unique(arr, arr+size);
        size = result - arr;
        if (size > max)
            cerr << "Out of Range error \n";
    }

    void Push(T elem)
    {
        arr[size] = elem;
        sort(arr, arr + size + 1);
        T* result = unique(arr, arr+size + 1);
        size = result - arr;
        if (size > max)
            cerr << "Out of Range error \n";
    }

    void Pop(int elem)
    {
        if (elem >=size)
            cerr << "Out of Range error \n";
        for (int i = elem; i< size - 1; i++)
        {
            arr[i] = arr[i+1];
        }
        size--;
    }

    void Print()
    {
        cout << "Size:" << size << endl;
        cout << "Array elements is:" << endl;
        for (unsigned int i = 0; i < size; i++) {
            cout << arr[i] << endl;
        }
        cout << endl;
    }

    int Size()
    {
        return size;
    }



    void operator = (const Set & other)
    {
        this->size = other.size;
        for (int i; i < other.size; i++)
        {
            this->arr[i] = other.arr[i];
        }
    }

    Set operator * (const Set & other)
    {
        Set<T,max> res;
        int s = size;
        res.size = s + other.size;
        int k = 0;
        for (int i = 0; i < s; i++)
        {
            for (int j = 0; j < other.size; j++)
            {
                if (arr[i] == other.arr[j])
                {
                    res.arr[k] = arr[i];
                    k++;
                }
            }
        }
        sort(res.arr, res.arr + k);
        T* result = unique(res.arr, res.arr+k);
        res.size = result - res.arr;
        if (res.size > max)
            cerr << "Out of Range error \n";
        return res;
    }

    Set operator + (const Set & other)
    {
        Set<T,max> res;
        int s = size;
        res.size = s + other.size;
        for (int i = 0; i < s; i++)
        {
            res.arr[i] = this->arr[i];
        }
        for (int i = s; i < res.size; i++)
        {
            res.arr[i] = other.arr[i - s];
        }
        sort(res.arr, res.arr + res.size);
        T* result = unique(res.arr, res.arr+res.size);
        res.size = result - res.arr;
        if (res.size > max)
            cerr << "Out of Range error \n";
        return res;
    }

    ~Set()
    {
        delete [] arr;
    }
};

class Time
{
    int min;
    int sec;
public:
    Time(int _min, int _sec)
    {
        min = _min;
        sec = _sec;
    }

    void Print()
    {
        cout << min << ':' << sec << endl;
    }
};

int main()
{
    // int
    cout << "int"<< endl << endl;
    int arr1[5] {0,2,2,5,7};
    int arr2[5] {0,9,2,11,7};
    Set<int, 10> a1(5, arr1);
    a1.Print();
    a1.Push(-1);
    a1.Print();
    a1.Pop(0);
    a1.Print();
    Set<int, 10> b1(5, arr2);
    b1.Print();
    Set<int, 10> c1 = a1 + b1;
    c1.Print();
    Set<int, 10> d1 = a1 * b1;
    d1.Print();
    cout << "Size: " << c1.Size() << endl;
    cout << endl;
// double
    cout << "double"<< endl << endl;
    double arr3[5] {0.1,2.1,2.1,5.1,7.1};
    double arr4[5] {0.1,9.1,2.1,11.1,7.1};
    Set<double, 10> a2(5, arr3);
    a2.Print();
    a2.Push(-1.7);
    a2.Print();
    a2.Pop(0);
    a2.Print();
    Set<double, 10> b2(5, arr4);
    b2.Print();
    Set<double, 10> c2 = a2 + b2;
    c2.Print();
    Set<double, 10> d2 = a2 * b2;
    d2.Print();
    cout << "Size: " << c2.Size() << endl;
    cout << endl;
// float
    cout << "float"<< endl << endl;
    double arr5[5] {0.1,2.1,2.1,5.1,7.1};
    double arr6[5] {0.1,9.1,2.1,11.1,7.1};
    Set<double, 10> a3(5, arr5);
    a3.Print();
    a3.Push(-1.7);
    a3.Print();
    a3.Pop(0);
    a3.Print();
    Set<double, 10> b3(5, arr6);
    b3.Print();
    Set<double, 10> c3 = a3 + b3;
    c3.Print();
    Set<double, 10> d3 = a3 * b3;
    d3.Print();
    cout << "Size: " << c3.Size() << endl;
    cout << endl;

//class Time
    Time now1(15,34);
    now1.Print();
    Time now2(16,34);
    now2.Print();
    Time now3(17,34);
    now3.Print();
    Time now4(18,34);
    now4.Print();
    Time now5(19,34);
    now5.Print();
    Time arr7[5] {now1, now2, now3, now4, now5};
}
